package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class JavaScriptAlertsPage extends BasePage{

    @FindBy(xpath = "/html/body/div[2]/div/div/ul/li[2]/button")
    protected WebElement jConfirm;

    @FindBy(id = "result")
    protected WebElement jResult;


    public JavaScriptAlertsPage(WebDriver driver) {
        super(driver);
    }

    public void confirm() {
        jConfirm.click();
    }

    public void javaScriptConfirm() {
        driver.switchTo().alert().accept();
    }

    public String getJSText() {
        return jResult.getText();
    }



}
