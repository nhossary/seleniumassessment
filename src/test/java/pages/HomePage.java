package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "/html/body/div[2]/div/ul/li[29]/a")
    protected WebElement jBody;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void submit(){
        jBody.click();
    }


}
