package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pages.BasePage;
import pages.HomePage;
import pages.JavaScriptAlertsPage;

class JSConfirmTest extends BaseTest {

    @Test
    public void JSAlertsConfirmTest()  {

        BasePage basePage = new BasePage(driver);
        basePage.GoToWebpage();

        HomePage homePage = new HomePage(driver);
        homePage.submit();

        JavaScriptAlertsPage javaAlertPage = new JavaScriptAlertsPage(driver);
        javaAlertPage.confirm();
        javaAlertPage.javaScriptConfirm();

        Assertions.assertEquals("You clicked: Ok", javaAlertPage.getJSText());


    }


}